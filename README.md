# Public GitLab Control Framwork

## Background

The resources contained in this repo were derived from [Adobe's CCF](https://blogs.adobe.com/security/2017/05/open-source-ccf.html) and adapted by GitLab in an attempt to make this tool easy to deploy within small companies without dedicated compliance personnel. The difficulty of security compliance is that, by the time an organization is ready to hire employees with knowledge of security compliance requirements, a lot of an organization's infrastructure has already been built. The goal of making some of these tools accessible is to lower the barrier to entry for security compliance, so companies can identify their future compliance needs early-on and build security control requirements into their architecture as they scale.

## Usage

* For the [csv file](./GitLab_Control_Framework_Baseline_Controls.csv) you can simply download the file, change the placeholders contained within brackets in the `Common Control Activity` column, and deploy as needed
* For the [Google Sheets document](https://docs.google.com/spreadsheets/d/1xmACTt5WD_u8OL0z8G0oUv7Di9J4DWHuy2kamch_7-g/edit#gid=907478311) there are instructions on the first sheet that walk you through how to customize the controls to your organization

## Future Plans

1. Create some tooling that will help leverage GitLab.com as a resource for getting started with deploying this control framework within your organization
1. Guidance about how to take this control framework and implement it within your organization and start rolling out security controls

## Related Resources

* A [blog post](https://about.gitlab.com/2019/05/07/choosing-a-compliance-framework/) talking about how GitLab went about chosing a control framework
* A blog post (coming soon) that describes how we adapted the Adobe CCF and created the GitLab Control Framework.

## Feedback

If you notice any errors in this project or have any comments/questions, please add a comment in the [feedback issue](https://gitlab.com/gitlab-com/gl-security/public-gcf/issues/1) for this repo.